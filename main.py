import os
import random
import sys

import pygame
from pygame.locals import *

from constants import *
import logging

from tile import Tile

logger = logging.getLogger(__name__)


def main():
    all_sprites = pygame.sprite.Group()
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH_IN_PIXELS, SCREEN_HEIGHT_IN_PIXELS))
    pygame.display.set_caption(GAME_NAME)

    for x in range(0, SCREEN_WIDTH_IN_TILES):
        for y in range(0, SCREEN_HEIGHT_IN_TILES):
            tile = random.randint(1, 71)
            Tile(tile, x, y, all_sprites)
    # Event loop
    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)

        all_sprites.update()
        screen.fill(BACKGROUND_COLOR)
        all_sprites.draw(screen)
        pygame.display.flip()


if __name__ == "__main__":
    main()

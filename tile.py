import logging
import os

from constants import *

import pygame

logger = logging.getLogger(__name__)


class Tile(pygame.sprite.DirtySprite):
    def __init__(self, number: int, x: int, y: int, *groups):
        super().__init__(*groups)
        if number < 1 or number > 71 or number == 36:
            number = 1
        number = str(number).zfill(2)
        path = os.path.join(
            "graphics", "rpg-pack", "tiles", "generic-rpg-tile{}.png".format(number)
        )
        try:
            image = pygame.image.load(path)
            if image.get_alpha() is None:
                self.image = image.convert()
            else:
                self.image = image.convert_alpha()
            self.image = pygame.transform.scale(self.image, (TILE_SIZE, TILE_SIZE))
            self.rect = self.image.get_rect()
            self.rect.x = x * TILE_SIZE
            self.rect.y = y * TILE_SIZE
        except pygame.error as message:
            logger.error("Can't load image {}".format(path))
            raise SystemExit(message)
